# Kubernetes for AI projects through examples

[Basic Setup] -> [Docker image] -> [Simple job in k8s] -> [Work with data in k8s jobs] -> [Simple monitoring of k8s]
 -> [Deployments in k8s]

Use cases:
> I want to create a development environment for my AI project.
    > I want to be able to specify which software / packages I need.
    > I want to be able to bring in large amounts of data.
    > I want to be able to store computed data in a persistent way.
    > I want to access this environment using `ssh`.
    > I want to be able to use Jupyer notebooks.

> I want to create a computing environment for my AI project (e.g. for job training).
    > I want to be able to specify which software / packages I need.
    > I want to be able to bring in large amounts of data.
    > I want to be able to store computed results in a persistent way.
    > I want to set up logging and monitoring
        > GPU utilization
        > wandb
        > grafana

> I want to set up a simple static web page



Intro - what is the purpose of this repo

## Basic setup
**Goal:** This and that works
- setup ssh keys
- install VSCode with remote ssh extension
    - why VSCode
    - maybe PyCharm works as well

## Create a docker image in GitLab
    - separate branch?

- set up .gitlab-ci.yml
    - When to run docker build?
- create Dockerfile
- check out container registry

## Use the built docker image in Kubernetes
    - separate branch?

- log into rancher
- add kubernetes config (download)
- create job.yaml and run the job
- check out the logs and status in CLI
- check out the logs and status in rancher

## Basic monitoring of resource usage in Kubernetes
- set up grafana
